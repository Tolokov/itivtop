CREATE DATABASE IF NOT EXISTS itivpro;

USE itivpro;

CREATE TABLE IF NOT EXISTS users(
    id BIGINT(20) AUTO_INCREMENT NOT NULL,
    first_name VARCHAR(30) NOT NULL,
    last_name VARCHAR(30) NOT NULL,
    user_name VARCHAR(30) NOT NULL,
    password VARCHAR(30) NOT NULL,
    location VARCHAR(30) NOT NULL,
    gender VARCHAR(30) NOT NULL,
    PRIMARY KEY (id)
);
