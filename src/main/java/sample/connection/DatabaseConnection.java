package sample.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static sample.config.DatabaseConfigs.*;

/**
 * @author Pavlo Khmarnyi
 * @since 11/6/2019
 */
public class DatabaseConnection {
    private Connection connection;

    public DatabaseConnection() {
        try {
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Error: " + e.getMessage());
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public void closeConnection() {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Error: " + e.getMessage());
        }
    }
}
