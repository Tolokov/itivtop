package sample.dao;

import sample.model.User;

/**
 * @author Pavlo Khmarnyi
 * @since 11/6/2019
 */
public interface UserDao {
    User getUserById(long id);
    User getUserByLogin(String login, String password);
    User getUserByName(String firstName, String lastName);
    Long saveUser(User user);
}
