package sample.dao.impl;

import sample.connection.DatabaseConnection;
import sample.dao.UserDao;
import sample.model.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static sample.util.Const.*;
import static sample.util.SqlStatements.*;

/**
 * @author Pavlo Khmarnyi
 * @since 11/6/2019
 */
public class UserDaoImpl implements UserDao {
    private DatabaseConnection databaseConnection;
    private PreparedStatement statement;
    private ResultSet resultSet;

    public UserDaoImpl(DatabaseConnection databaseConnection) {
        this.databaseConnection = databaseConnection;
    }

    @Override
    public User getUserById(long id) {
        User user = null;
        try {
            statement = databaseConnection.getConnection().prepareStatement(SELECT_USER_BY_ID);
            statement.setLong(1, id);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                user = new User(
                        resultSet.getLong(USERS_ID),
                        resultSet.getString(USERS_FIRSTNAME),
                        resultSet.getString(USERS_LASTNAME),
                        resultSet.getString(USERS_USERNAME),
                        resultSet.getString(USERS_PASSWORD),
                        resultSet.getString(USERS_LOCATION),
                        resultSet.getString(USERS_GENDER)
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Error: " + e.getMessage());
        } finally {
            finalizeConnection();
        }

        return user;
    }

    @Override
    public User getUserByLogin(String login, String password) {
        User user = null;
        try {
            statement = databaseConnection.getConnection().prepareStatement(SELECT_USER_BY_LOGIN);
            statement.setString(1, login);
            statement.setString(2, password);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                user = new User(
                        resultSet.getLong(USERS_ID),
                        resultSet.getString(USERS_FIRSTNAME),
                        resultSet.getString(USERS_LASTNAME),
                        resultSet.getString(USERS_USERNAME),
                        resultSet.getString(USERS_PASSWORD),
                        resultSet.getString(USERS_LOCATION),
                        resultSet.getString(USERS_GENDER)
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Error: " + e.getMessage());
        } finally {
            finalizeConnection();
        }

        return user;
    }

    @Override
    public User getUserByName(String firstName, String lastName) {
        User user = null;
        try {
            statement = databaseConnection.getConnection().prepareStatement(SELECT_USER_BY_NAME);
            statement.setString(1, firstName);
            statement.setString(2, lastName);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                user = new User(
                        resultSet.getLong(USERS_ID),
                        resultSet.getString(USERS_FIRSTNAME),
                        resultSet.getString(USERS_LASTNAME),
                        resultSet.getString(USERS_USERNAME),
                        resultSet.getString(USERS_PASSWORD),
                        resultSet.getString(USERS_LOCATION),
                        resultSet.getString(USERS_GENDER)
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Error: " + e.getMessage());
        } finally {
            finalizeConnection();
        }

        return user;
    }

    @Override
    public Long saveUser(User user) {
        Long id = null;
        try {
            statement = databaseConnection.getConnection().prepareStatement(INSERT_USER, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, user.getFirstName());
            statement.setString(2, user.getLastName());
            statement.setString(3, user.getLogin());
            statement.setString(4, user.getPassword());
            statement.setString(5, user.getCountry());
            statement.setString(6, user.getGender());
            statement.executeUpdate();

            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                id = resultSet.getLong(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Error: " + e.getMessage());
        } finally {
            finalizeConnection();
        }

        return id;
    }

    private void finalizeConnection() {
        try {
            if (resultSet != null) {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }
            databaseConnection.closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Error: " + e.getMessage());
        }
    }
}
