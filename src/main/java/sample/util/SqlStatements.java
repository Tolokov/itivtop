package sample.util;

import static sample.util.Const.*;

/**
 * @author Pavlo Khmarnyi
 * @since 11/6/2019
 */
public class SqlStatements {
    public static final String SELECT_USER_BY_ID = "SELECT * FROM "
            + USER_TABLE
            + " WHERE id = ?";
    public static final String SELECT_USER_BY_LOGIN = "SELECT * FROM "
            + USER_TABLE
            + " WHERE "
            + USERS_USERNAME
            + " = ? AND "
            + USERS_PASSWORD
            + " = ?";
    public static final String SELECT_USER_BY_NAME = "SELECT * FROM "
            + USER_TABLE
            + " WHERE "
            + USERS_FIRSTNAME
            + " = ? AND "
            + USERS_LASTNAME
            + " = ?";
    public static final String INSERT_USER = "INSERT INTO "
            + USER_TABLE
            + "( " + USERS_FIRSTNAME
            + ", " + USERS_LASTNAME
            + ", " + USERS_USERNAME
            + ", " + USERS_PASSWORD
            + ", " + USERS_LOCATION
            + ", " + USERS_GENDER + ") "
            + "VALUES(?, ?, ?, ?, ?, ?)";
}
