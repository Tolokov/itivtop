package sample.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import sample.model.User;
import sample.service.LoginService;
import sample.service.impl.LoginServiceImpl;

import java.net.URL;
import java.util.ResourceBundle;

public class SingUpController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField loginField;

    @FXML
    private PasswordField passwordField;

    @FXML
    private Button signUpButton;

    @FXML
    private TextField signUpLastName;

    @FXML
    private TextField singUpName;

    @FXML
    private TextField singUpCountry;

    @FXML
    private RadioButton singUpCheckBoxButtonMale;

    @FXML
    private RadioButton singUpCheckBoxFemale;

    private LoginService loginService;

    @FXML
    void initialize() {
        loginService = new LoginServiceImpl();
        signUpButton.setOnAction(event -> {
            User user = new User();
            user.setFirstName(singUpName.getText());
            user.setLastName(signUpLastName.getText());
            user.setLogin(loginField.getText());
            user.setPassword(passwordField.getText());
            user.setCountry(singUpCountry.getText());
            user.setGender("Male");
            loginService.signUp(user);
        });
    }
}
