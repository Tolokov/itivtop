package sample.service.impl;

import sample.connection.DatabaseConnection;
import sample.dao.UserDao;
import sample.dao.impl.UserDaoImpl;
import sample.model.User;
import sample.service.LoginService;

/**
 * @author Pavlo Khmarnyi
 * @since 11/6/2019
 */
public class LoginServiceImpl implements LoginService {
    private UserDao userDao;

    public LoginServiceImpl() {
        userDao = new UserDaoImpl(new DatabaseConnection());
    }

    @Override
    public boolean signUp(User user) {
        Long id = userDao.saveUser(user);
        return id != null;
    }

    @Override
    public boolean loginUser(String login, String password) {
        User user = userDao.getUserByLogin(login, password);
        return user != null;
    }
}
