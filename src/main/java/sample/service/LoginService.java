package sample.service;

import sample.model.User;

/**
 * @author Pavlo Khmarnyi
 * @since 11/6/2019
 */
public interface LoginService {
    boolean signUp(User user);
    boolean loginUser(String login, String password);
}
